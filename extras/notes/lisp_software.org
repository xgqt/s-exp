#+TITLE: Software using Lisp or Scheme

#+AUTHOR: Maciej Barć
#+LANGUAGE: en

#+STARTUP: showall inlineimages
#+OPTIONS: toc:nil num:nil
#+REVEAL_THEME: black


* Sources


  - [[https://github.com/ghosthamlet/awesome-lisp-machine][Awesome: Lisp Machines]]
  - [[https://github.com/schemedoc/awesome-scheme][Awesome: Schemes]]
  - [[https://packages.gentoo.org/][Gentoo: Package Index]]
  - [[https://www.gimp.org/tutorials/Basic_Scheme/][GIMP: intro to Scheme]]
  - [[https://www.gnu.org/software/guile/docs/guile-tut/tutorial.html][GNU: Scheme + C tuturial]]
  - [[https://www.gnu.org/software/guile/libraries/][GNU: Software that uses Guile]]
  - [[https://en.wikipedia.org/wiki/GNU_Guile][Wikipedia: GNU Guile]]
  - also [[https://github.com][GitHub]] and the Internet ;)


* Languages


** Lisp

*** SBCL

   Uses [[https://github.com/sbcl/sbcl/][SBCL]] (dev-lisp/sbcl)

| software category | the package name | git repository of the package or project website |
|-------------------+------------------+--------------------------------------------------|
| dev-lang          | cl-python        | https://github.com/metawilm/cl-python/           |
| gui-libs          | mcclim           | https://common-lisp.net/project/mcclim/          |
| sci-mathematics   | acl2             | https://www.cs.utexas.edu/users/moore/acl2/      |
| sci-mathematics   | maxima           | https://github.com/andrejv/maxima/               |
| sci-mathematics   | pvs              | https://github.com/SRI-CSL/PVS                   |
| sys-apps          | clawk            | https://github.com/sharplispers/clawk            |
| x11-wm            | stumpwm          | https://github.com/stumpwm/stumpwm/              |

*** Self-hosted

   Uses a built-in compiler or one built by that software project

| software category | the package name | git repository of the package or project website |
|-------------------+------------------+--------------------------------------------------|
| app-editors       | emacs            | https://github.com/emacs-mirror/emacs/           |


** Scheme

*** Guile

   Uses [[https://www.gnu.org/software/guile/][GNU Guile]] (dev-scheme/guile)

| software category | the package name | git repository of the package or project website |
|-------------------+------------------+--------------------------------------------------|
| app-admin         | shroud           | https://dthompson.us/projects/shroud.html        |
| app-office        | gnucash          | https://github.com/Gnucash/gnucash/              |
| app-office        | texmacs          | https://github.com/texmacs/texmacs/              |
| dev-scheme        | akku             | https://github.com/weinholt/akku/                |
| dev-scheme        | guile-newt       | https://gitlab.com/mothacehe/guile-newt/         |
| games-board       | aisleriot        | https://gitlab.gnome.org/GNOME/aisleriot/        |
| gui-libs          | guile-gnome      | https://www.gnu.org/software/guile-gnome/        |
| media-gfx         | graphviz         | https://gitlab.com/graphviz/graphviz/            |
| media-sound       | lilypond         | https://github.com/lilypond/lilypond/            |
| sci-electronics   | geda             | http://wiki.geda-project.org/                    |
| sci-electronics   | lepton-eda       | https://github.com/lepton-eda/lepton-eda/        |
| sci-misc          | opencog          | https://github.com/opencog/opencog               |
| sys-apps          | guix             | https://github.com/guix-mirror/guix/             |
| sys-devel         | autogen          | https://www.gnu.org/software/autogen/            |
| www-apps          | haunt            | https://dthompson.us/projects/haunt.html         |
| www-client        | nomad            | https://www.nongnu.org/nomad/                    |
| x11-wm            | guile-wm         | https://github.com/mwitmer/guile-wm/             |

*** SCM

   Uses [[https://people.csail.mit.edu/jaffer/SCM/][SCM]] (dev-scheme/scm)

| software category | the package name | git repository of the package or project website |
|-------------------+------------------+--------------------------------------------------|
| sci-mathematics   | freesnell        | https://people.csail.mit.edu/jaffer/FreeSnell/   |

*** Racket

    Uses [[https://racket-lang.org/][Racket]] (dev-scheme/racket)

| software category | the package name  | git repository of the package or project website       |
|-------------------+-------------------+--------------------------------------------------------|
| app-doc           | htdp              | https://github.com/racket/htdp                         |
| app-shells        | rash              | https://github.com/willghatch/racket-rash              |
| dev-games         | r-cade            | https://github.com/massung/r-cade                      |
| dev-lang          | anarki            | https://github.com/arclanguage/anarki                  |
| dev-util          | mred-designer     | https://github.com/Metaxal/MrEd-Designer               |
| media-gfx         | identikon         | https://github.com/DarrenN/identikon/                  |
| net-misc          | riposte           | https://github.com/vicampo/riposte                     |
| sci-mathematics   | cas               | https://github.com/soegaard/racket-cas                 |
| sci-mathematics   | faster-minikanren | https://github.com/michaelballantyne/faster-miniKanren |
| sci-mathematics   | medikanren        | https://github.com/webyrd/mediKanren                   |
| sci-mathematics   | rascas            | https://github.com/Metaxal/rascas                      |
| sys-apps          | xiden             | https://github.com/zyrolasting/xiden                   |
| www-apps          | frog              | https://github.com/greghendershott/frog                |
| www-apps          | thenotepad        | https://github.com/otherjoel/thenotepad                |
| www-apps          | travis-racket     | https://github.com/greghendershott/travis-racket       |

*** Self-hosted

   Uses a built-in compiler or one built by that software project

| software category | the package name | git repository of the package or project website |
|-------------------+------------------+--------------------------------------------------|
| dev-util          | schism           | https://github.com/google/schism/                |
| media-gfx         | gimp             | https://github.com/GNOME/gimp/                   |
| x11-wm            | sawfish          | https://github.com/SawfishWM/sawfish/            |


* Compilers


** Popular

   The most popular lisp or scheme compilers or interpreters

| implementation of | the package name | git repository of the package or project website |
|-------------------+------------------+--------------------------------------------------|
| lisp              | clisp            | https://clisp.sourceforge.io/                    |
| lisp              | sbcl             | https://github.com/sbcl/sbcl/                    |
| scheme            | chez             | https://github.com/cisco/ChezScheme/             |
| scheme            | chicken          | http://www.call-cc.org/                          |
| scheme            | gambit           | https://github.com/gambit/gambit                 |
| scheme            | guile            | https://www.gnu.org/software/guile/              |
| scheme            | racket           | https://github.com/racket/racket/                |
| scheme            | scm              | https://people.csail.mit.edu/jaffer/SCM/         |


** Interesting

   Interesting projects implementing or extending lisp or scheme

| implementation of | the package name | git repository of the package or project website |
|-------------------+------------------+--------------------------------------------------|
| lisp              | clasp            | https://github.com/clasp-developers/clasp/       |
| lisp              | fennel           | https://github.com/bakpakin/Fennel/              |
| lisp              | lispex           | https://github.com/kedebug/LispEx/               |
| scheme            | gerbil           | https://github.com/vyzo/gerbil                   |
