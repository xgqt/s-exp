#+TITLE: Lisp Docs

#+AUTHOR: Maciej Barć
#+LANGUAGE: en

#+STARTUP: showall inlineimages
#+OPTIONS: toc:nil num:nil
#+REVEAL_THEME: black


* Lisp


** Common Lisp HyperSpec

   + Main:
     - http://www.lispworks.com/documentation/HyperSpec/Front/X_Mast_9.htm


* Scheme


** Racket

   + Main:
     - https://docs.racket-lang.org/

   + Macros:
     - https://www.greghendershott.com/fear-of-macros/


* "Lisp"


** Julia

   + Main:
     - https://docs.julialang.org/
     - https://docs.julialang.org/en/v1/base/base/#
