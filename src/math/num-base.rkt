#lang racket/base


(struct num (symb base ints) #:transparent)

;; ints are reverse
;; #< + 10 '(1 2 3) >

(define (num->dec anum)
  {define symb (num-symb anum)}
  {define base (num-base anum)}
  (for/sum ([int (in-list (num-ints anum))] [idx (in-naturals)])
    (symb (* (expt base idx) int))
    ))

(define (dec+base->num number base)
  {define (recc acc num)
    (cond
      [(zero? num)  acc]
      [else
       (recc (append acc (list (remainder num base))) (quotient num base))]
      )}
  {define symb (if (> number 0) + -)}
  (num symb base  (recc '() number))
  )
