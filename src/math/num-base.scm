;; Convert between numerical systems


(define (dec->base number base)
  (letrec
      ((recc
        (lambda (acc num)
          (cond
           ((zero? num)  acc)
           (else  (recc (cons (remainder num base) acc) (quotient num base)))
           )
          )
        ))
    (recc '() number)
    )
  )

;; (dec->base 19 9) -> '(2 1)
