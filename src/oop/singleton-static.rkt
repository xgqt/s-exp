#lang racket


(define singleton%
  (let ([x 1]
        [y 2])
    (class object%
      (super-new)

      (define/public (get-x)
        x)

      (define/public (set-x v)
        (set! x v))

      (define/public (get-y)
        y)

      (define/public (set-y v)
        (set! y v)))))


(define si-1 (new singleton%))

(define si-2 (new singleton%))


(send si-1 set-x 999)

(send si-2 get-x)
