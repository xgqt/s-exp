#lang racket


(define make-singleton-object
  (let ([instance #false]
        [singleton%
         (class object%
           (super-new)

           (inspect #false)

           (init-field value))])
    (lambda (value)
      (unless instance
        (set! instance (new singleton% [value value])))
      instance)))


(define si-1 (make-singleton-object 1))

(define si-2 (make-singleton-object 2))


(set-field! value si-1 3)

(get-field value si-2)
