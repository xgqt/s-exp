#!/usr/bin/env racket


#|
Check if object(s) is/are something
in other words
Check if a check performed by check-procedure on objects returns true

Example:
(check-object number? 1)  ==  check if 1 is a number

Position of check-procedure and objects is reversed because
0 or more objects can be given and check-procedure is mandatory.
|#


#lang racket/base

(require
 racket/contract/base
 )

(provide
 (contract-out
  [check-object
   (->* ((procedure-arity-includes/c 1)) (#:else any/c) #:rest any/c any/c)]
  )
 )


(define (check-object check-procedure #:else [else-result #f] . v)
  {define (check v)
    (if (check-procedure v) v else-result)
    }
  (cond
    ;; no objects given
    [(null? v)  else-result]
    ;; exactly one object given
    [(= 1 (length v))  (check (car v))]
    ;; >1 objects given
    [else  (map check v)]
    )
  )


(module+ test
  (require rackunit)

  ;; null
  (check-eq?  (check-object number?)  #f)
  (check-eq?  (check-object number? #:else 'nan)  'nan)

  ;; one
  (check-eq?  (check-object number? 1)  1)
  (check-eq?  (check-object number? #t)  #f)
  (check-eq?  (check-object (lambda (v) (= 1 v)) 1)  1)
  (check-eq?  (check-object (lambda (v) (= 1 v)) 0)  #f)
  (check-eq?  (check-object number? #f #:else 'nan)  'nan)
  (check-eq?  (check-object number? 1 #:else 'nan)  1)
  (check-eq?  (check-object number? '() #:else 'nan)  'nan)

  ;; many
  (check-equal?  (check-object number? 1 2 #:else 'nan)  '(1 2))
  (check-equal?  (check-object number? 1 #f #:else 'nan)  '(1 nan))
  )
