#lang typed/racket/base

(provide (all-defined-out))


(define-type XExpr-Attribute
  (List Symbol String)
  )

;; Assumes permissive-xexprs is False
(define-type XML-Content
  (U cdata comment element entity p-i pcdata)
  )

(define-type XML-Misc
  (U comment p-i)
  )

(define-type XExpr
  (U
   XML-Misc cdata Positive-Index
   Number String Symbol
   (Pair Symbol (Pair (Listof XExpr-Attribute) (Listof XExpr)))
   (Pair Symbol (Listof XExpr))
   ))


(require/typed/provide xml
  [#:struct location
   ([line   : (U False Exact-Nonnegative-Integer)]
    [char   : (U False Exact-Nonnegative-Integer)]
    [offset : Exact-Nonnegative-Integer])]
  [#:struct source
   ([start : location]
    [stop  : location])]
  [#:struct external-dtd
   ([system : String])]
  [#:struct (external-dtd/public external-dtd)
   ([public : String])]
  [#:struct (external-dtd/system external-dtd)
   ()]
  [#:struct document-type
   ([name     : Symbol]
    [external : external-dtd]
    [inlined  : False])]
  [#:struct comment
   ([text : String])]
  [#:struct (p-i source)
   ([target-name : Symbol]
    [instruction : String])]
  [#:struct prolog
   ([misc  : (Listof XML-Misc)]
    [dtd   : (U document-type False)]
    [misc2 : (Listof XML-Misc)])]
  [#:struct document
   ([prolog  : prolog]
    [element : element]
    [misc    : (Listof XML-Misc)])]
  [#:struct (element source)
   ([name       : Symbol]
    [attributes : (Listof attribute)]
    [content    : (Listof XML-Content)])]
  [#:struct (attribute source)
   ([name  : Symbol]
    [value : Any])]
  [#:struct (entity source)
   ([text : (U Symbol Positive-Index)])]
  [#:struct (pcdata source)
   ([string : String])]
  [#:struct (cdata source)
   ([string : String])]
  [valid-char?  (-> Any Boolean)]
  [xexpr?  (-> Any Boolean)]
  [read-xml  (->* () (Input-Port) document)]
  [read-xml/document  (->* () (Input-Port) document)]
  [read-xml/element  (->* () (Input-Port) element)]
  [write-xml  (->* (document) (Output-Port) Void)]
  [write-xml/content  (->* (XML-Content) (Output-Port) Void)]
  [display-xml
   (->* (document) (Output-Port #:indentation (U 'none 'classic 'peek 'scan)) Void)]
  [display-xml/content
   (->* (XML-Content) (Output-Port #:indentation (U 'none 'classic 'peek 'scan)) Void)]
  [write-xexpr  (->* (XExpr) (Output-Port #:insert-newlines? Any) Void)]
  [permissive-xexprs  (Parameterof False)]
  [xml->xexpr  (-> XML-Content XExpr)]
  [xexpr->xml  (-> XExpr XML-Content)]
  [xexpr->string  (-> XExpr String)]
  [string->xexpr  (-> String XExpr)]
  [xml-attribute-encode  (-> String String)]
  [eliminate-whitespace
   (->* () ((Listof Symbol) (-> Boolean Boolean)) (-> element element))]
  [validate-xexpr  (-> Any True)]
  [correct-xexpr?  (-> Any (-> Any) (-> Any Any) Any)]
  [current-unescaped-tags  (Parameterof (Listof Symbol))]
  [html-unescaped-tags  (Listof Symbol)]
  [empty-tag-shorthand  (Parameterof (U 'always 'never (Listof Symbol)))]
  [html-empty-tags  (Listof Symbol)]
  [collapse-whitespace  (Parameterof Boolean)]
  [read-comments  (Parameterof Boolean)]
  [xml-count-bytes  (Parameterof Boolean)]
  [xexpr-drop-empty-attributes  (Parameterof Boolean)]
  )


(define-type Se-Path
  (Listof (U Symbol Keyword))
  )

(require/typed/provide xml/path
  [se-path*/list  (-> Se-Path XExpr (Listof Any))]
  [se-path*  (-> Se-Path XExpr Any)]
  )
