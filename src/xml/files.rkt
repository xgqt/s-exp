#lang racket

(require
 xml
 )

(provide (all-defined-out))


(define (read-xml-file path)
  (read-xml (open-input-string (file->string path)))
  )

(define (xml-file->xexpr path)
  (string->xexpr (file->string path))
  )
