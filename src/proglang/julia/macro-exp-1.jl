#!/usr/bin/env julia


#=
https://jkrumbiegel.com/pages/2021-06-07-macros-for-beginners/

macro fill(exp, sizes...)

    iterator_expressions = map(sizes) do s
        Expr(
            :(=),
            :_,
            quote 1:$(esc(s)) end
        )
    end

    Expr(
        :comprehension,
        esc(exp),
        iterator_expressions...
    )
end

=#

#=
# v...  <=> ,@(v)
a = [1, 2, 3]
[a..., 4, 5]  # =>  [1, 2, 3, 4, 5]
=#


function list(arg ...)
    return [arg ...]
end

list(1,2)  # =>  2-element Vector{Int64}: 1 2


#=
((n) -> (n + n)).([1, 2, 3])  # =>  3-element Vector{Int64}: 2 4 6
(n -> begin (n + n) end).([1, 2, 3])
map([1, 2, 3]) do n ; (n + n) end
=#


macro l1(id ...)
    :( $( list(id ...) ) )
end

@l1 1 2  # =>  2-element Vector{Int64}: 1 2
@l1(1, 2)[1]  # =>  1

@macroexpand @l1 1 2 3  # =>  3-element Vector{Int64}: 1 2 3


# eval(quote 1 end)  # =>  1


macro l2(id ...)
    :( [ (((i) -> ([i, i])).($id)) ... ] )
end

macro l3(id...)
    :( [ $( (i -> [i, i]).(id) ... ) ] )
end

@macroexpand @l3 1 2 3  #  =>  :([[1, 1], [2, 2], [3, 3]])

macro l4(id...)
    :( $( (i -> [i, i]).(id) ) )
end

typeof(@l4 1 2 3)  #  =>  Tuple{Vector{Int64}, Vector{Int64}, Vector{Int64}}
