#!/usr/bin/env racket


#|
Emulating a "Expert System" in Racket

References:
- http://www.clipsrules.net/
- http://clipsrules.sourceforge.net/documentation/v640/ug.pdf
|#


#lang racket/base

(require
 racket/class
 racket/set
 "sat.rkt"
 )

(provide inference)


(define inference
  (class* object% ()
    (super-new)

    (field
     [fact-list (mutable-set)]
     [rule-list (make-hash)]
     [break-list (mutable-set)]
     [watch-facts? #f]
     [watch-activations? #f]
     )

    {define halt? #f}
    {define activated-list (mutable-set)}

    (define/private (watch/unwatch what bool)
      (case what
        [(activations) (set! watch-activations? #t)]
        [(facts) (set! watch-facts? #t)]
        [(all)
         (for-each (lambda (w) (watch/unwatch w bool)) '(activations facts))]
        ))

    (define/public (watch what)
      (watch/unwatch what #t)
      )

    (define/public (unwatch what)
      (watch/unwatch what #f)
      )

    (define/public (assert fact)
      (when (not (list? fact))
        (error 'fact-error "Not a valid fact: ~a" fact)
        )
      (when (not (and halt? (set-member? fact-list fact)))
        (set-add! fact-list fact)
        (when watch-facts? (printf "==> ~a\n" fact))
        )
      )

    (define/public (retract fact)
      (set-remove! fact-list fact)
      (when watch-facts? (printf "<== ~a\n" fact))
      )

    (define/public (facts)
      {define n
        (for/fold ([sum 0]) ([fact (in-set fact-list)])
          (printf "f-~a  ~a\n" sum fact)
          (add1 sum)
          )}
      (printf "For a total of ~a facts.\n" n)
      )

    (define/public (defrule name condition action)
      (hash-set! rule-list name (cons condition action))
      )

    (define/public (rules)
      {define n
        (for/fold ([sum 0]) ([(rule-name _) (in-hash rule-list)])
          (printf "r-~a  ~a\n" sum rule-name)
          (add1 sum)
          )}
      (printf "For a total of ~a defrules.\n" n)
      )

    (define/public (reset)  ; clears facts
      (set! fact-list (mutable-set))
      )

    (define/public (clear)  ; clear rules & facts
      (set! rule-list (make-hash))
      (set! activated-list (mutable-set))
      (send this reset)
      )

    (define/public (set-break rulename)
      (set-add! break-list rulename)
      )

    (define/public (remove-break rulename)
      (set-remove! rulename)
      (set! halt? #f)
      )

    (define/public (run)
      (for ([fact (in-set fact-list)])
        (for ([(rule-name rule-c/a) (in-hash rule-list)])
          (cond
            [halt? (void)]  ; skip if in halt-mode
            [(set-member? activated-list rule-name) (void)]  ; skip if activated
            [(set-member? break-list rule-name)  ; activate halt-mode
             (printf "Break on ~a" rule-name)
             (set! halt? #t)]
            [else
             {define s (sat? (car rule-c/a) fact)}
             (when s
               (when watch-activations?
                 (printf "Activation ~a: ~a\n" rule-name fact))
               (set-add! activated-list rule-name)
               (apply (cdr rule-c/a) s)
               )]
            ))
        ))

    ))


(module+ test

  (require
   rackunit
   racket/port
   )

  {define f (new inference)}

  (send f defrule 'quack '(animal-is duck) (lambda () (display "quack")))
  (send f assert '(duck))
  (send f defrule 'duck '(duck) (lambda () (display "duck")))
  (send f assert '(animal-is duck))
  (check-equal? (with-output-to-string (lambda () (send f run))) "quackduck")

  (send f retract '(duck))
  (send f assert '(duck))
  (check-equal? (with-output-to-string (lambda () (send f run))) "")

  )
