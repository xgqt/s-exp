;; CL implementation by "*no-defun-allowed*", thanks!


(defmacro d-def (expr)
  `(setf (fdefinition
          ',(intern (string-downcase (prin1-to-string expr)))) ,expr))


(d-def (lambda (x) (+ x 1)))
(|(lambda (x) (+ x 1))| 1)
