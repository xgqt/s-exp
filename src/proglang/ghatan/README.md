# Ghatan Language


## About

This project is a scratchpad for design & implementation
of the Ghatan Programming Language.

It is inspired by
[Boogie](https://github.com/boogie-org/boogie) and
[Dafny](https://github.com/dafny-lang/dafny) languages.

Name is taken from Ghatanothoa,
the Great Old One from the Lovecraft's Mythos
https://en.wikipedia.org/wiki/Xothic_legend_cycle
