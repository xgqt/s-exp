// Original from: https://docs.racket-lang.org/inside/cs-embedding.html


#include <string.h>

// Headers from: /usr/include/racket
#include "racket/chezscheme.h"
#include "racket/racketcs.h"

#include "ctool/base.c"


static ptr to_bytevector(char *s);


static ptr to_bytevector(char *s)
{
    iptr len = strlen(s);
    ptr bv = Smake_bytevector(len, 0);

    memcpy(Sbytevector_data(bv), s, len);

    return bv;
}


int main(int argc, char *argv[])
{
    racket_boot_arguments_t ba;
    memset(&ba, 0, sizeof(ba));

    ba.boot1_path = "./boot/petite.boot";
    ba.boot2_path = "./boot/scheme.boot";
    ba.boot3_path = "./boot/racket.boot";
    ba.exec_file = argv[0];

    racket_boot(&ba);
    declare_modules();
    racket_namespace_require(Sstring_to_symbol("racket/base"));

    {
        for (int i = 1; i < argc; i++) {
            ptr e = to_bytevector(argv[i]);
            e = Scons(Sstring_to_symbol("open-input-bytes"), Scons(e, Snil));
            e = Scons(Sstring_to_symbol("read"), Scons(e, Snil));
            e = Scons(Sstring_to_symbol("eval"), Scons(e, Snil));
            e = Scons(Sstring_to_symbol("println"), Scons(e, Snil));
            racket_eval(e);
        }
    }
    {
        ptr rbase_sym = Sstring_to_symbol("racket/base");
        ptr repl_sym = Sstring_to_symbol("read-eval-print-loop");

        racket_apply(Scar(racket_dynamic_require(rbase_sym, repl_sym)), Snil);
    }

    return 0;
}
