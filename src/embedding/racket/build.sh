#!/usr/bin/env bash


# Requires:
#  - Racket CS (Racket on Chez Scheme)
#  - Racket with --enable-libs -- this builds libracketcs.a

# Warning: your installation directories may be different!


# Cleanup
for d in bin boot ctool
do
    [ -d ./"${d}" ] && rm -rf ./"${d}"
    mkdir -p ./"${d}"
done

# Copy boot files
cp /usr/lib64/racket/*.boot ./boot/

# Generate base.c
raco ctool ++lib racket/base --c-mods ctool/base.c

flags=(
    -o bin/main  # output binary
    -lracketcs   # use libracketcs.a
    -ldl
    -lm
    -lpthread
)

# Compile
cc main.c "${flags[@]}"
