(define-syntax struct
  (lambda (stx)
    (syntax-case stx ()
      ((_ struct-name (field-name ...))
       #'(begin
           (define (struct-name field-name ...)
             (lambda (action)
               (case action
                 ((set)
                  (lambda (field-name-symbol value)
                    (case field-name-symbol
                      ((field-name)
                       (set! field-name value)) ...
                       (else
                        (error "unknown field-name" field-name-symbol)))))
                 ((get)
                  (lambda (field-name-symbol)
                    (case field-name-symbol
                      ((field-name)
                       field-name) ...
                       (else
                        (error "unknown field-name" field-name-symbol)))))
                 ((list)
                  (lambda ()
                    (list field-name ...)))
                 (else
                  (error "unknown action" action))))))))))

(define (struct->list struct-object)
  ((struct-object 'list)))

(define-syntax-rule (struct-get struct-object field-name)
  ((struct-object 'get) 'field-name))

(define-syntax-rule (struct-set! struct-object field-name field-value)
  ((struct-object 'set) 'field-name field-value))


;; Tests:

(struct sample-struct (a b c))

(define A
  (sample-struct 0 2 3))

((A 'set) 'a 1)

((A 'get) 'a)

(struct-set! A b 222)

(struct-get A b)

(struct->list A)
