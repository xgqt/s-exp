#|
Use places for IO workloads.
BTW: Futures block IO.
https://docs.racket-lang.org/guide/parallelism.html
|#


#lang racket/base

(require racket/place)

(provide main)


(define (any-double? l)
  (for/or ([i (in-list l)])
    (for/or ([i2 (in-list l)])
      (printf "~v~%" i)
      (printf "~v~%" i2)
      (= i2 (* 2 i)))))


(define (main)
  (let ([worker
         (place worker-channel
           (let* ([worker-input (place-channel-get worker-channel)]
                  ;; ^ Get worker's channel input.
                  ;; Do a calculation on the input.
                  [worker-output (any-double? worker-input)])
             (place-channel-put worker-channel worker-output)))])
    (place-channel-put worker (list 1 2 4 8))
    (place-channel-get worker)))

;; (map place-wait list-of-places)
