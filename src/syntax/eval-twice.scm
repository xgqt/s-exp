(define-syntax-rule (test-#1 a)
  (+ a a))

(test-#1 (begin
           (display "foo\n")
           3))

;; => foo foo 6


(define-syntax-rule (test-#2 a)
  (let ([a-evaled a])
    (+ a-evaled a-evaled)))

(test-#2 (begin
           (display "foo\n")
           3))

;; => foo 6
