;; Pipes (threading macros)
;; R7RS
;; Inspired by: https://docs.racket-lang.org/heresy/pipes.html


;; kawa
;; (import (srfi 1))

;; guile
;; (use-modules (srfi srfi-1))


;; Compose FUNS in reverse order and apply them to INITIAL

;; foldl
(define (:> initial . funs)
  (fold (lambda (fun val) (fun val)) initial funs))

;; compose
;; (define (:> initial . funs)
;;   ((apply compose funs) initial))


;; taken argument is placed FIRST in function call
(define-syntax f>
  (syntax-rules ()
    ((_ fun args ...)
     (lambda (x)
       (fun x args ...)))))

;; taken argument is placed LAST in function call
(define-syntax l>
  (syntax-rules ()
    ((_ fun args ...)
     (lambda (x)
       (fun args ... x)))))


(define-syntax ->
  (syntax-rules ()
    ((_ initial (fun args ...) ...)
     (:> initial
         (f> fun args ...)
         ...))))

(define-syntax ->>
  (syntax-rules ()
    ((_ initial (fun args ...) ...)
     (:> initial
         (l> fun args ...)
         ...))))


;; (define (add1 num) (+ 1 num))
;;
;; (:> 1 add1 add1 number->string)
;;
;; (->> (list 1 2 3)
;;      (map (lambda (x) (* x x)))
;;      (append '(3))
;;      (apply +)
;;      (add1))
