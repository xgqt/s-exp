;; Loop macro in standard Scheme

;; We use cond because it is guaranteed to be available and "if" macro can
;; sometimes require a then-expression as well as a else-expression.

;; Tested with: chez, guile, and racket.

;; Test:
;; (loop 3 (display "Hello!\n"))


(define-syntax loop
  (syntax-rules ()
    ((_ iterations expression ...)
     (let loop ([stop-counter iterations])
       (cond
        ((> stop-counter 0)
         expression ...
         (loop (- stop-counter 1))))))))
