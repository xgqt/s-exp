(define-syntax swap!
  (syntax-rules ()
    ((_ idA idB)
     (let ((tmp idA))
       (set! idA idB)
       (set! idB tmp)))))

(define (swap-parameter idA idB)
  (let ((tmp (idA)))
    (idA (idB))
    (idB tmp)))
