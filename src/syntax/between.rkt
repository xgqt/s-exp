#!/usr/bin/env racket

#lang racket/base


;; Easy mode

(define-syntax-rule (between (vA proc vB) ...)
  (values (proc vA vB) ...))


;; Hard Mode

;; transform each function call


;; Tests

(module+ test
  (require rackunit)

  (check-true  (between (0 = 0)))
  (check-=  (between (1 + 2))  3  0)
  (check-=  (between (8 / 2))  4  0)
  )
