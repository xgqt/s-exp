;; Generic `plus' and `minus' functions
;; generated via `generic' macro


(define-syntax generic
  (syntax-rules ()
    ((_ (contract procedure) ...)
     (lambda (v . vs)
       (cond
        ((contract v)
         (apply procedure v vs)) ...
        (else
         (error "Unsupported type.")))))))


(define plus
  (generic
   (list?   append)
   (null?   append)
   (number? +)
   (string? string-append)))

(define minus
  (generic
   (number?  -)
   ;; other clauses are not so portable :(
   ))
