#lang racket/base

(require
 syntax/parse/define
 (for-syntax racket/base))

(provide (rename-out (if-then if)))


(begin-for-syntax
  (define-syntax-class elemnt-group
    (pattern {multi-element ...}
             #:when (eq? (syntax-property this-syntax 'paren-shape) #\{)
             #:with expression
             #'(begin multi-element ...))
    (pattern single-element
             #:with expression
             #'single-element)))

(define-syntax-parse-rule
  (if-then if-expression
           {~datum then} then-expression:elemnt-group
           {~datum else} else-expression:elemnt-group)
  (if if-expression
      then-expression.expression
      else-expression.expression))


;; => 2

(if-then #t
 then (add1 1)
 else (add1 10))

;; => 1

(if-then #t
 then {
     add1
     1
 }
 else {
     add1
     10
 })
