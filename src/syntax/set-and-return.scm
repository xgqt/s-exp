;; Set! and return

;; Line C++ post- and pre- incrementation


;; v++ -- return old and then set!

(define-syntax post-set!
  (syntax-rules ()
    ((_ id v)
     (let ((old-id-v id))
       (set! id v)
       old-id-v))))


;; ++v -- set! and then return new

(define-syntax pre-set!
  (syntax-rules ()
    ((_ id v)
     (begin
       (set! id v)
       id))))
