#!/usr/bin/env julia


# WARNING!!!: Julia indexes starting from 1


function median_grouped(vect)
    len = length(vect)

    cfs = let sum = 0
        map(function (f)
                sum = sum + f[2]
                sum
            end,
            vect)
    end

    n2 = cfs[len] / 2

    class = 0
    for (idx, val) in enumerate(cfs)
        if val > n2
            class = idx
            break
        end
    end

    Cb = vect[class][1]
    L = Cb[1]            # lower boundary of class
    F = cfs[class - 1]   # cumulative frequency before class
    fm = vect[class][2]  # frequency of median class
    C = Cb[2] - Cb[1]    # class length

    L + C * (n2 - F) / fm
end


#=
V = [[[20,  25],  2],
     [[25,  30],  5],
     [[30,  35],  8],
     [[35,  40],  10],
     [[40,  45],  7],
     [[45,  50],  10],
     [[50,  55],  3],]
median_grouped(V)
@assert median_grouped(V) == 38.75
=#
